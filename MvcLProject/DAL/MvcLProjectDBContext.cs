﻿using MvcLProject.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MvcLProject.DAL
{
    public class MvcLProjectDBContext : DbContext
    {
        public MvcLProjectDBContext()
            : base("name=MvcLProjectDBContext")
        {

        }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Product> Products { get; set; }

    }

}